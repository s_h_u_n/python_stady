# surasura python capture9

# ライブラリ
import requests

# from ライブラリ import 関数
# BeautifulSoup:HTML解析用ライブラリ
from bs4 import BeautifulSoup

# url先を変数へ
result = requests.get('https://pycon.jp/2016/ja/schedule/talks/list/')

# BeautifulSoup(解析するHTML, 解析する処理の種類)
soup = BeautifulSoup(result.text, 'html.parser')

#　divタグのclass=presentationを先頭１つ抜き出す
#p = soup.find('div', class_='presentation')

# 全要素取得
presentation_list = soup.find_all('div', class_='presentation')
print(len(presentation_list))

# print(' Language | Title ')
# print('------------------')

# capture 10
with open('scr.csv', 'w') as f:

    # mac だと改行に 「\(option + ¥)」を使う必要がある.
    f.write("{0},{1}\n".format('Language','Title'))

    for i in presentation_list:
        # タグで絞り込み,htmlのテキストのみを取得.
        txt = i.h3.get_text()

        if '(en)' in txt:
            lang = "English"
            # スペース(¥xa0)削除.
            tt = txt.replace("(en)", "")

        elif '(ja)' in txt:
            lang = "Japanese"
            tt = txt.replace("(ja)", "")

        # 『{0:n}』 nの幅で文字を埋め込む.
        # 『:<n』 左詰n文字以内.
        # 『:^n』 中央n文字以内.
        #print("{0:<10}| {1}".format(lang, tt))
        f.write("{0},{1}\n".format(lang, tt))