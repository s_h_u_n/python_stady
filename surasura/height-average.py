# surasura python

heights = {'taro':168, 'jiro':171, 'sabu':165}

total = 0
for i in heights.values():
    total += i

average = total / len(heights)

# itos: str(int), stoi: int(str)
print('ave:{0}cm.'.format(average))

# check test 2
Hey = 'Hello world'
print(Hey.replace('Hello', 'cello'))

# check test 3
number1 = 5
number2 = 10
# かつ: and 或いは: or
if number1 == 5 or number2 == 19:
    print('true')
else:
    print('false')

# check test 4
num_list = [1,2,3]
for i in num_list:
    print(i)

# check test 5
alpha_num_dick = {'a':1, 'b':2, 'c': 3}
alpha_num_dick['a'] = 10
i=0
for key, val in alpha_num_dick.items():
    print('dic_no{0} : {1} | {2}'.format(i, key, val))
    # incliment/ dicriment : +=1/-=1
    i+=1

# check test 6
def print_square(num):
    return num * num
print(print_square(5))

# check test 7
def add_10(num):
    try:
        add_num = num + 10
        print('add_num : {}'.format(add_num))
        return add_num
    except:
        print('error')
add_10('10')

def add_20(num):
    # isinstance(変数, 型): 第一引数の型と第二引数の指定型が一致していればTrue.
    if not isinstance(num, int):
        print('Invalid num')
        return False
    add_num = num + 20
    print('add_num : {}'.format(add_num))
    return add_num
add_20('20')